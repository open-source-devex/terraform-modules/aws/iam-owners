#!/usr/bin/env sh

set -e

set -v

CI_VARIABLES=ci-variables.tfvars
OVERRIDES_FILE=ci-overrides.tf

echo '
aws_access_key_id = "string"
aws_secret_access_key = "string"
aws_region = "string"
bucket_name = "string"
users = ["list"]
' >> ${CI_VARIABLES}

echo 'provider "aws" {
  region     = "string"
  access_key = "string"
  secret_key = "string"
}
' >  ${OVERRIDES_FILE}

terraform init
terraform validate -var-file ${CI_VARIABLES} .

rm -f ci-*.tf*
