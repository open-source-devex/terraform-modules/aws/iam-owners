data "aws_caller_identity" "current" {}

resource "aws_iam_role" "owner" {
  name = "owner"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": {
    "Effect": "Allow",
    "Action": "sts:AssumeRole",
    "Principal": {
      "AWS": "arn:aws:iam::${data.aws_caller_identity.current.account_id}:root"
    }
  }
}
EOF
}

resource "aws_iam_role_policy_attachment" "owner_aws_portal" {
  role       = aws_iam_role.owner.name
  policy_arn = aws_iam_policy.aws_portal.arn
}

resource "aws_iam_role_policy_attachment" "owner_admin" {
  role       = aws_iam_role.owner.name
  policy_arn = "arn:aws:iam::aws:policy/AdministratorAccess"
}
