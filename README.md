# aws-iam-owners

Terraform module to setup owners role.
In addition to admin right owners also have access to features of the AWS portal that are usually reserved for the root account.
A user assuming the owner role can for instance see the billing dashboard for the account.

## Usage

```hcl-terraform
module "iam_owners" {
  source = "git://gitlab.com/open-source-devex/terraform-modules/aws/iam-owners.git?ref=v1.0.1"
}
```
