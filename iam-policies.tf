resource "aws_iam_policy" "aws_portal" {
  name        = "aws-portal"
  path        = "/"
  description = "Policy to enable access to all features of aws portal"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "aws-portal:*",
        "budgets:*",
        "cur:*"
      ],
      "Effect": "Allow",
      "Resource": [
        "*"
      ]
    }
  ]
}
EOF
}
